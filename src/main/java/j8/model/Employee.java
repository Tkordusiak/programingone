package j8.model;

public class Employee {

    String firstName;
    String lastName;
    int age;
    String proffesion;
    double salary;
    ContractType type;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getProffesion() {
        return proffesion;
    }

    public void setProffesion(String proffesion) {
        this.proffesion = proffesion;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public ContractType getType() {
        return type;
    }

    public void setType(ContractType type) {
        this.type = type;
    }

    public Employee(String firstName, String lastName, int age, String profession, double salary, ContractType type) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.proffesion = profession;
        this.salary = salary;
        this.type = type;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "firstName='" + firstName + '\'' +
                ", LastName='" + lastName + '\'' +
                ", age=" + age +
                ", proffesion='" + proffesion + '\'' +
                ", salary=" + salary +
                ", type=" + type +
                '}';
    }
}
