package j8.model;

public enum ContractType {
    F("Full"),
    H("Half");


    private  String desc;

    ContractType(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}
