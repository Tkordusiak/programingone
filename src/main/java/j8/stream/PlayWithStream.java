package j8.stream;

import j8.model.ContractType;
import j8.model.Employee;

import java.nio.file.Path;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

public class PlayWithStream {

    public static void main(String[] args) {

        List<Employee> employees = FileUtills.load(Path.of("src/main/resources/j8/employees.csv"));


//        empSalary(employees);
//        ampAge(employees);
//        empSka(employees);
//        empName(employees);
//        ex5(employees);
//        ex6(employees);
//         ex7(employees);
//        ex8(employees);
//         ex9(employees);
//       ex10(employees);
        //ex11(employees);
//        ex12(employees);
//        ex13(employees);
//        ex14(employees);
//        ex14a(employees);
//        ex14c(employees);
//        ex14d(employees);
        ex15(employees);


    }

    // IntStatistic

    private static void ex15(List<Employee> employees) {
        Optional<Employee> collect = employees.stream()

                .max(Comparator.comparing(e -> e.getSalary()
                ));
        System.out.println(collect);

        System.out.println(employees.stream()
                .mapToDouble(e->e.getSalary())
                .summaryStatistics());
    }

    /**
     * zamiana doubla na int( metoda collectingAndThen
     *
     * @param employees
     */
    private static void ex14d(List<Employee> employees) {
        Map<String, Integer> collect = employees.stream()
                .collect(groupingBy(e -> e.getFirstName(), //mapa gdzie kluczem jest String
                        collectingAndThen(
                                counting(), //tu jest Long
                                value -> value.intValue() //konwersja z Long na Intiger
                        )

                        )
                );
        System.out.println(collect);
    }


    private static void ex14c(List<Employee> employees) {
        Map<String, Long> a = employees.stream()
                .collect(groupingBy(e -> e.getFirstName(),
                        filtering(
                                e -> !e.getFirstName().endsWith("a"),
                                counting()
                        )
                        )
                );
        System.out.println(a
        );
    }

    private static void ex14a(List<Employee> employees) {
        Map<String, Long> collect = employees.stream()
                .collect(groupingBy(e -> e.getFirstName(),
                        counting()

                        )
                );
        System.out.println(collect);
    }

    /**
     * Stwórz mapę gdzie kluczem będzie Imię a wartościa Lista Employess
     *
     * @param employees
     */
    private static void ex14(List<Employee> employees) {
        Map<String, List<Employee>> collect = employees.stream()
                .collect(groupingBy(e -> e.getFirstName()
                        )
                );
        System.out.println(collect);
    }

    /**
     * Wypisz średnią zarobków osób pracujących na pełny/pół etaptu
     *
     * @param employees
     */
    private static void ex13(List<Employee> employees) {
        Map<String, Double> collect = employees.stream()
                .collect(
                        groupingBy(e -> e.getType().name(),
                                averagingDouble(e -> e.getSalary()
                                )
                        )
                );
        System.out.println(collect);
    }


    /**
     * Wypisz ilość osób zarabiających wiecęj niż 5000, w formacie true->20, false->30 (patritinioningBy() + couting())
     *
     * @param employees
     */
    private static void ex12(List<Employee> employees) {
        Map<Boolean, Long> collect = employees.stream()
                .collect(Collectors.partitioningBy(e -> e.getSalary() > 5000,
                        Collectors.counting()
                        )
                );
        System.out.println(collect);
    }

    /**
     * Wypisz wszystkie nazwiska w formacie {lastName,lastName}
     *
     * @param employees
     */
    private static void ex11(List<Employee> employees) {
        String collect = employees.stream()
                .map(e -> e.getLastName())
                .collect(Collectors.joining(","));

        System.out.println(collect);

    }

    private static void ex9(List<Employee> employees) {
        employees.stream()
                .filter(e -> e.getFirstName().charAt(2) == 'a')
                .filter(e -> e.getLastName().charAt(2) == 'b')
                .forEach(System.out::println);
    }

    /**
     * Posortuj listę osób w następujący sposób - wpierw nazwisko alfabetycznie rosnąco, następnie imię.
     *
     * @param employees
     */
    private static void ex10(List<Employee> employees) {
        employees.stream()
                .sorted(Comparator.comparing(Employee::getLastName)
                        .thenComparing(Employee::getFirstName))
//                        (person1, person2) -> {
//                            int lastNameComarator =
//                                    person1.getLastName()
//                                            .compareTo(person2.getLastName());
//
//                            if (lastNameComarator != 0) {
//                                return lastNameComarator;
//                            }
//                            return person1.getFirstName()
//                                    .compareTo(person2.getFirstName());
//                        }


                .forEach(System.out::println);

    }


    /**
     * Zasymuluj podwyżki pracowników o 12%,
     * tworząc wynikową mapę której kluczem będzie nazwisko a wartościa wynagrodzenie po podwyżce
     *
     * @param employees
     */
    private static void ex8(List<Employee> employees) {

        Map<String, List<Double>> collect = employees.stream()
                .collect(
                        groupingBy(
                                Employee::getLastName,
                                mapping(e -> e.getSalary() * 1.12,
                                        Collectors.toList()
                                )
                        )
                );
        System.out.println(collect);

    }


    /**
     * zad 7
     * Sprawdź czy w liscie instnieje osoba o nazwisku "Kowalski", jedną instrukcją
     *
     * @param employees
     */
    private static void ex7(List<Employee> employees) {
        boolean isKowalski = employees.stream().anyMatch(e -> e.getFirstName().equals("Kowalski"));
        System.out.println(isKowalski);
    }

    /**
     * zad 6
     * Wypisz profesje wszystkich pracownikow z dużych liter
     *
     * @param employees
     */
    private static void ex6(List<Employee> employees) {
        employees.stream()
                .map(Employee::getProffesion)
                .map(String::toUpperCase)
                .forEach(System.out::println);

    }


    /**
     * zad 5
     * 3 ostatnie litery z naziwska, jeśli kończa się na ski/ska maja być z dużych liter
     */
    private static void ex5(List<Employee> employees) {
        employees.stream()
                .map(Employee::getLastName)
                .map(e -> e.substring(e.length() - 3))
                .map(PlayWithStream::toUppSkiSka)
                .forEach(System.out::println);

    }


    /**
     * jesli booolen zwróci true podniśki do UpperCaase jeśli false to nic nie robi
     *
     * @param e
     * @return
     */
    private static String toUppSkiSka(String e) {
        return (e.endsWith("ski") || e.endsWith("ska")) ? e.toUpperCase() : e;
    }


    /**
     * zad 4
     * imiona wszystkich pracowników
     */
    private static void empName(List<Employee> employees) {
        employees.stream()
                .map(Employee::getFirstName)
                .forEach(System.out::println);
    }


    /**
     * zad 3
     * osoby których nazwisko kończy się na ska i pracują na cały etat
     */
    private static void empSka(List<Employee> employees) {
        List<Employee> empSka = employees.stream()
                .filter(p -> p.getLastName().endsWith("ska"))
                .filter(p -> p.getType().equals(ContractType.F))
                .collect(Collectors.toList());
        System.out.println(empSka);
    }

    /**
     * zad 2
     * osoby o wieku parzystym
     */
    private static void ampAge(List<Employee> employees) {
        List<Employee> empAge = employees.stream()
                .filter(p -> p.getAge() % 2 == 0)
                .collect(Collectors.toList());
        System.out.println(empAge);
    }

    /**
     * zad 1
     * osoby których wynagorodzenie jest w przedziale 2500-3199
     */
    private static void empSalary(List<Employee> employees) {
        List<Employee> empSalary = employees.stream()
                .filter(salaryBeetwen2500_3199())
                .collect(Collectors.toList());
        System.out.println(empSalary);
    }


    private static Predicate<Employee> salaryBeetwen2500_3199() {
        return p -> p.getSalary() > 2500 && p.getSalary() < 3199;
    }
}
