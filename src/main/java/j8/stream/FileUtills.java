package j8.stream;

import j8.model.ContractType;
import j8.model.Employee;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileUtills {

    public static List<Employee> load(Path path) {
        try {
            Stream<String> linesOfCsvFiles = Files.lines(path);
            return linesOfCsvFiles.skip(1)
                    .map(e -> e.trim())
                    .map(e -> e.split(","))
                    // .map(e -> mapToObject(e))
                    .map(FileUtills::mapToObject)
                    //.forEach(e -> System.out.println(e))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public static Employee mapToObject(String[] empArray) {
        String firstName = empArray[0];
        String lastName = empArray[1];
        int age = Integer.parseInt(empArray[2]);
        String proffesion = empArray[3];
        double salary = Double.parseDouble(empArray[4]);
        ContractType contractType = ContractType.valueOf(empArray[5]);

        Employee employee = new Employee(firstName, lastName, age, proffesion, salary, contractType);

        return employee;
    }

}

