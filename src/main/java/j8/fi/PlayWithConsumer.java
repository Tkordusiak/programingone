package j8.fi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class PlayWithConsumer {

    public static void main(String[] args) {

        // lambda idzie do wersji consumer
        Consumer<String> consumer = System.out::println;
        Consumer<String> consumerLambda = (String t) -> System.out.println(t);

        consumer.accept("coś");

        List<String> stringList = new ArrayList<>(Arrays.asList("Asia", "Kasia", "Basia"));
        Consumer<List<String>> listConsumer = List::clear;
       // listConsumer.accept(stringList);
        System.out.println(stringList.size());

        MyConsumer<List<String>> addConsumer = (List<String> t) -> t.add("test");
        MyConsumer<List<String >> secondConsumer = (List<String > t) -> t.add(" hello");
        MyConsumer<List<String>> groupingConsumer = addConsumer.andThen(secondConsumer);
        groupingConsumer.accept(stringList);
        System.out.println(stringList);

    }
}
