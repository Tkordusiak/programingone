package j8.fi;

import java.util.Objects;

@FunctionalInterface
public interface MyConsumer<T> {

    void accept(T t);

    default MyConsumer<T> andThen(MyConsumer<T> other) {
        return (T t) -> {
            Objects.requireNonNull(other);
            this.accept(t);
            other.accept(t);
        };

    }


}
