package j8.fi;

import java.util.function.Predicate;

public class PlayWithPredicate {
    public static void main(String[] args) {

        Predicate<String> checkA = (String t) -> t.contains("A");
        System.out.println(checkA.test("Ania"));
        System.out.println(checkA.test("Yeti"));

        Predicate<String> isEmpty = (String s) -> s.isEmpty();
        System.out.println(isEmpty.test(""));

        Predicate<String> hasAAndIsEmpty = checkA.and(isEmpty);
        System.out.println(hasAAndIsEmpty.test("test"));
    }


}
