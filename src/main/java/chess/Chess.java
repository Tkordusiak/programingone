package chess;

import java.util.Scanner;

public class Chess {

    public static final String WITHE_FILED = " \u25A1 ";
    public static final String BLACK_FILED = " \u25A0 ";


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj długość szachownicy");
        int size = sc.nextInt();
        System.out.println("Rozmiar szachownicy = " + size);
        for (int y = 0; y < size; y++) {
            for (int x = 0; x < size; x++) {
                //przechodzimy do nowych wierszy
                if ((y+x) % 2 == 0) {
                    System.out.print(WITHE_FILED);
                } else {
                    System.out.print(BLACK_FILED);
                }
            }
            //znak nowej lini
            System.out.println();
        }
    }
}
