package hospital;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Witaj w rejestracji");

        String option ;
        HospitalQueueService hospitalQueueService = new HospitalQueueService();
        do{
            printMeanu();
            option = sc.nextLine();
            if ("a".equals(option)) {
                System.out.println("Rejestracja nowego pacjęta");
                Patient newPatient= handleNewPatient(sc);
                hospitalQueueService.addPatient(newPatient);
            } else if ("b".equals(option)) {
                Patient handlePatient = hospitalQueueService.handlePatient();
                printPatientInfo(handlePatient);
            } else if ("c".equals(option)) {
                System.out.println(hospitalQueueService.queueSize());
            } else if ("d".equals(option)) {
                System.out.println(hospitalQueueService.nextPatient());

            }
        }while (!"q".equals(option));

    }

    private static void printMeanu() {
        System.out.println("Co robimy? \n" + "Wybierz: \n" + "a - Rejesteracja nowego pacjęta \n"
                + "b - Obsłuż pacjęta \n" +"c - Ilość pacjętów \n" + "d - Następny pacjęt \n" + "q - Wyjście \n");
    }

    private static void printPatientInfo(Patient handlePatient) {
        System.out.println(new StringBuffer()
                .append("Pacjent ")
                .append(handlePatient.getFirstName())
                .append(" ")
                .append(handlePatient.getLastName())
                .append("Został przyjety ")
                .toString()
        );
    }

    private static Patient handleNewPatient(Scanner sc) {
        System.out.println("Imię: ");
        String name = sc.nextLine();
        System.out.println("Nazwisko: ");
        String surname = sc.nextLine();
        System.out.println("Złość: ");
        int howAngry = sc.nextInt();
        System.out.println("Choroba ");
        sc.nextLine();
        String diseaseSytringValue = sc.nextLine();
        Disease disease  =Disease.valueOf(diseaseSytringValue);

        Patient patient = new Patient();
        patient.setFirstName(name);
        patient.setLastName(surname);
        patient.setHowAngry(howAngry);
        patient.setDisease(disease);
        System.out.println("Zarejestrowano pacjenta");
        return patient;
    }
}
