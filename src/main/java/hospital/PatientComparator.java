package hospital;

import java.util.Comparator;

public class PatientComparator implements Comparator<Patient> {

    private static final String KOWALSKI = "Kowalski";

    @Override
    public int compare(Patient o1, Patient o2) {
        boolean isFirstKowalski = o1.getLastName().equals(KOWALSKI);
        boolean isSecondKowalski = o2.getLastName().equals(KOWALSKI);
        if (!isFirstKowalski && isSecondKowalski) {
            return 1;
        } else if (isFirstKowalski && !isSecondKowalski) {
            return -1;
        } else {
            boolean isO1Sthserious = o1.getDisease().equals(Disease.STH_SERIOUS);
            boolean isO2Sthserious = o2.getDisease().equals(Disease.STH_SERIOUS);

            if (!isO1Sthserious && isO2Sthserious) {
                return -1;
            } else if (isO1Sthserious && !isO2Sthserious) {
                return -1;

            } else {
                Integer o1Factor = o1.getDisease().getInfectiousness() * o1.getHowAngry();
                Integer o2Factor = o2.getDisease().getInfectiousness() * o1.getHowAngry();

                return o1Factor.compareTo(o2Factor);
            }
        }

    }
}
