package hospital;

import java.util.PriorityQueue;

public class HospitalQueueService {

    private PriorityQueue<Patient> hospitalQueue = new PriorityQueue<>(new PatientComparator());


    public void addPatient(Patient patient){
        hospitalQueue.add(patient);
    }
     //poll
    public Patient handlePatient(){
        return hospitalQueue.poll();
    }

    //peek
    public Patient nextPatient(){
        return hospitalQueue.peek();
    }

    public int queueSize(){
        return hospitalQueue.size();
    }

}
