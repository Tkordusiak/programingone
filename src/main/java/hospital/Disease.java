package hospital;

/**
 * 1-low
 * 4-high
 */
public enum Disease {
    FLU(1),
    COLD(2),
    DIARRHEA(3),
    STH_SERIOUS(4);

    private int infectiousness;

    Disease(int infectiousness) {
        this.infectiousness = infectiousness;
    }

    public int getInfectiousness() {
        return infectiousness;
    }
}
