package factorial;

import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj liczbę");
        int n = sc.nextInt();
        System.out.println(n +"! = " + factorial(n));

    }
     static int factorial(int n){
        if (n>=1){
            return n*factorial(n-1);
        }else {
            return 1;
        }
    }

}
