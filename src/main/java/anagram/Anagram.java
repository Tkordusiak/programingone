package anagram;

import java.util.Arrays;
import java.util.Scanner;

public class Anagram {
    public static void main(String[] args) {
        System.out.println("Podaj dwa słowa ");
        Scanner sc = new Scanner(System.in);
        System.out.println("Pierwsze słowo");
        String firstWord = sc.nextLine();
        System.out.println("podaj drugie słowo");
        String secondWord = sc.nextLine();
        System.out.println("Podałeś: " + firstWord + ", " + secondWord);


        char[] firstWordCharakter = firstWord.toCharArray();
        char[] secondWordCharakter = secondWord.toCharArray();
        Arrays.sort(firstWordCharakter);
        Arrays.sort(secondWordCharakter);
        boolean arrayEquels = Arrays.equals(firstWordCharakter, secondWordCharakter);

        if (arrayEquels){
            System.out.println("Są anagramami");
        }else {
            System.out.println("Nie są anagramy");
        }

    }
}
