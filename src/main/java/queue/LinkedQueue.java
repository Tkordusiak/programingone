package queue;

public class LinkedQueue<T> implements Queue<T> {

    private Link<T> head;

// napisac metodę size i napisać implemetacje doubleLinkedQueue


    @Override
    public void offer(T element) {
        Link<T> newLink = new Link<>(element);
        if (head == null) {
            head = newLink;
        } else {
            Link<T> link = this.head;
            while (link.getNext() != null) {
                link = link.getNext();
            }
            link.setNext(newLink);
        }
    }

    @Override
    public T poll() {
        if (head == null){
            System.out.println("Queue is empty");
            return null;
        }
        T headValue = head.getValue();
        head = head.getNext();
        return headValue;
    }

    @Override
    public T peek() {
        return (T) head.getValue();
    }

    @Override
    public int size() {
        return 0;
    }
}
