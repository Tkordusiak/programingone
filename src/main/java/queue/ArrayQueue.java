package queue;

import java.util.Arrays;
import java.util.Objects;

public class ArrayQueue<T> implements Queue<T> {

    private Object[] elements;

    public ArrayQueue() {
        this.elements = new Object[0];
    }

    @Override
    public void offer(T element) {
        elements = Arrays.copyOf(elements, elements.length + 1);
        elements[elements.length - 1] = element;
    }

    @Override
    public T poll() {
        if (checkIsEmpty()) return null;
        T element = (T) elements[0];
        elements = Arrays.copyOfRange(elements, 1, elements.length);
        return element;
    }


    @Override
    public T peek() {
        if (checkIsEmpty()) return null;
        T element = (T) elements[0];
        return element;
    }

    @Override
    public int size() {
        return elements.length;
    }

    private boolean checkIsEmpty() {
        if (elements.length == 0) {
            System.out.println("Queue is empty");
            return true;
        }
        return false;
    }
}
