package sort;

public class BubbleSorter implements Sorter {
    @Override
    public void sort(int[] toSort) {
        for (int i = 1; i < toSort.length; i++) {
            for (int j = 0; j < toSort.length - 1; j++) {
                // [4,2,1,19]
                // [2,4,1,19]
                if (toSort[j] > toSort[j + 1]) {
                    ArrayUtils.swapElements(toSort, j, j + 1);
                }
            }
        }

    }

    @Override
    public String getAlgorithmName() {
        return "Sortowanie bąbelkowe";
    }
}
