package sort;

public class InsertionSorter implements Sorter {
    @Override
    public void sort(int[] toSort) {
        for (int i = 1; i < toSort.length ; i++) {
            for (int j = i - 1; j >= 0; j--) {
                if (toSort[j]>toSort[j+1]){
                      ArrayUtils.swapElements(toSort, j, j+1);
                }else {
                    break;
                }

            }
//            int j = i;
//            while (j>0 && toSort[j-1]>toSort[j]){
//            ArrayUtils.swapElements(toSort, j, j-1);
//            j=j -1;
//        }
        }
    }

    @Override
    public String getAlgorithmName() {
        return "Sortowanie insertion";
    }
}
