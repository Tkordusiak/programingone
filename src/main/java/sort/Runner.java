package sort;

import java.util.Arrays;

public class Runner {
    public static void main(String[] args) {
        int[] ints = ArrayUtils.generateIntegerRandomArray(100);

        System.out.println(Arrays.toString(ints));

        Sorter bubel = new BubbleSorter();
        Sorter insert = new InsertionSorter();
        insert.sort(ints);
        System.out.println("Po sortowaniu");
        System.out.println(Arrays.toString(ints));



    }
}
