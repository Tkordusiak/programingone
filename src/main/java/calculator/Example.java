package calculator;

public enum Example {
    RED("Czerwony"){
        @Override
        public void something() {
            System.out.println("Something RED");
        }
    },
    GREEN("Zielony"){
        @Override
        public void something() {
            System.out.println("Something GREEN");
        }
    },
    BLUE("Niebieski"){
        @Override
        public void something() {
            System.out.println("Something BLUE");
        }
    };

    private  String desc;

    Example(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public abstract void something();
}
