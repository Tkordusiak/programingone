package calculator;

import java.util.function.IntBinaryOperator;

public enum Calkulator {
    ADD("dodawanie", new IntBinaryOperator() {
        @Override
        public int applyAsInt(int left, int right) {
            return left + right;
        }
    }),
    SUBSTRACT("odejmowanie", (x, y) -> x - y),
    MUTIPLICATION("mnożenie", (x, y) -> x * y);


    private String desc;
    private IntBinaryOperator operator;

    Calkulator(String desc, IntBinaryOperator operator) {
        this.desc = desc;
        this.operator = operator;
    }

    public int calculate(int A, int B) {
        return operator.applyAsInt(A, B);
    }

    ;

}
