package advendOfCode;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day1 {
// (masa/3->wynik zaokrąglić w dół)-2 --->paliwo
    // liczymy sla każdego wpisu z pliku liczymy paliwo
    // sumujemy wszystko
    // poprawa literówki

    public static void main(String[] args) throws IOException {
        Stream<String> input = Files.lines(Path.of("src/main/resources/advendOfCode/advenOfCode"));
        List<String> listOfModuleMass = input.collect(Collectors.toList());

        int sum = 0;

            for (String mass: listOfModuleMass) {
            int requiredFuel = calculateFuel(Integer.parseInt(mass));
            sum += requiredFuel;
            System.out.println("Fules needed for mass " + mass + " --> " + requiredFuel);
        }
        System.out.println("Sum: " + sum);

    }

    public static int calculateFuel(int mass) {
        int fuel = (mass / 3) - 2;
        if(fuel <= 0){
            return 0;
        }else {
            return fuel + calculateFuel(fuel);
        }
    }
}
