package lock;


import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        System.out.println("Podaj kombinacje zamka w formacie 1-2-4");
        Scanner sc = new Scanner(System.in);
        String[] lockCombination = sc.nextLine().split("-");

        Lock lock = new Lock(
                Integer.parseInt(lockCombination[0]),
                Integer.parseInt(lockCombination[1]),
                Integer.parseInt(lockCombination[2])
        );
        System.out.println(lock);
        lock.shuffled();
        lock.switchA();
        lock.switchB();
        lock.switchC();
        System.out.println(lock);

        if (lock.isOpen()){
            System.out.println("zamek jest otwarty");
        }else {
            System.out.println("zamek jest zamknięty");
        }
    }
}
